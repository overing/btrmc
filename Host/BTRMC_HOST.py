#!/usr/bin/python
# -*- coding:utf-8 -*-

# Depend software
# Python 2.6:
#   http://www.python.org/download/releases/2.6.6/
#
# PyBluez 0.18:
#   http://code.google.com/p/pybluez/downloads/list

from bluetooth import *
from ctypes import windll
from Tkinter import *
import SendKeysCtypes
import socket
import thread
import time
import traceback

# Windows Media Control Const reference:
#   http://msdn.microsoft.com/en-us/library/windows/desktop/ms646275(v=vs.85).aspx
WM_APPCOMMAND                  = 0x0319
APPCOMMAND_VOLUME_DOWN         = 0x00090000
APPCOMMAND_VOLUME_UP           = 0x000A0000
APPCOMMAND_MEDIA_NEXTTRACK     = 0x000B0000
APPCOMMAND_MEDIA_PREVIOUSTRACK = 0x000C0000
APPCOMMAND_MEDIA_STOP          = 0x000D0000
APPCOMMAND_MEDIA_PLAY_PAUSE    = 0x000E0000

SendMessage = windll.user32.SendMessageA

hwnd = 0

def playPause():
    # 千千 GHK: C+A+F5
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_CTRL"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_F5"])
    # time.sleep(.25)
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_F5"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_CTRL"])
    
    # Windows Media Control
    SendMessage(hwnd, WM_APPCOMMAND, hwnd, APPCOMMAND_MEDIA_PLAY_PAUSE)

def stop():
    # 千千 GHK: C+A+F6
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_CTRL"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_F6"])
    # time.sleep(.25)
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_F6"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_CTRL"])
    
    # Windows Media Control
    SendMessage(hwnd, WM_APPCOMMAND, hwnd, APPCOMMAND_MEDIA_STOP)

def volumeUp():
    # 千千 GHK: C+A+Up
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_CTRL"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_UP"])
    # time.sleep(.25)
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_UP"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_CTRL"])
    
    # Windows Media Control
    SendMessage(hwnd, WM_APPCOMMAND, hwnd, APPCOMMAND_VOLUME_UP)

def volumeDown():
    # 千千 GHK: C+A+Down
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_CTRL"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_DOWN"])
    # time.sleep(.25)
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_DOWN"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_CTRL"])
    
    # Windows Media Control
    SendMessage(hwnd, WM_APPCOMMAND, hwnd, APPCOMMAND_VOLUME_DOWN)

def previous():
    # 千千 GHK: C+A+Left
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_CTRL"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_LEFT"])
    # time.sleep(.25)
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_LEFT"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_CTRL"])
    
    # Windows Media Control
    SendMessage(hwnd, WM_APPCOMMAND, hwnd, APPCOMMAND_MEDIA_PREVIOUSTRACK)

def next():
    # 千千 GHK: C+A+Right
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_CTRL"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.PressKey(SendKeysCtypes.CODES["VK_RIGHT"])
    # time.sleep(.25)
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_RIGHT"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_ALT"])
    # SendKeysCtypes.ReleaseKey(SendKeysCtypes.CODES["VK_CTRL"])
    
    # Windows Media Control
    SendMessage(hwnd, WM_APPCOMMAND, hwnd, APPCOMMAND_MEDIA_NEXTTRACK)

def handleCommand(cmd):
    if cmd == "CMD_PLAY_PAUSE":
        playPause()
    elif cmd == "CMD_STOP":
        stop()
    elif cmd == "CMD_PREVIOUS":
        previous()
    elif cmd == "CMD_NEXT":
        next()
    elif cmd == "CMD_VOL_UP":
        volumeUp()
    elif cmd == "CMD_VOL_DOWN":
        volumeDown()

def handleConnect(server_sock):
    while True:
        client_sock, client_info = server_sock.accept()
        print "Accepted connection from ", client_info
        try:
            while True:
                data = client_sock.recv(1024)
                if len(data) == 0:
                	break
                print "received %s" % data
                handleCommand(data)
        except IOError:
            pass
        print "Disconnected from ", client_info
        client_sock.close()
    server_sock.close()

def BTServer(name):
    uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
    server_sock = BluetoothSocket(RFCOMM)
    server_sock.bind(("", PORT_ANY))
    server_sock.listen(1)
    ch = server_sock.getsockname()[1]
    advertise_service(server_sock, "BTRMCServer", 
            service_id=uuid, 
            service_classes=[uuid, SERIAL_PORT_CLASS], 
            profiles=[SERIAL_PORT_PROFILE], 
            protocols=[OBEX_UUID])
    print "Waiting for connection on RFCOMM channel %d" % ch
    handleConnect(server_sock)
    print "BTServer done"

def WifiServer(name):
    port = 8080
    server_sock = socket.socket()
    server_sock.bind(("", port))
    server_sock.listen(1)
    print "Waiting for connection on Wifi localhost:%d" % port
    handleConnect(server_sock)
    print "WifiServer done"

class MainFrame(Frame):
    def createWidgets(self):
        PLAY_PAUSE = Button(self, text="Play/Pause", command=playPause)
        PLAY_PAUSE.pack(side=LEFT)
        
        STOP = Button(self, text="Stop", command=stop)
        STOP.pack(side=LEFT)
        
        VOL_UP = Button(self, text="Vol+", command=volumeUp)
        VOL_UP.pack(side=LEFT)
        
        VOL_DOWN = Button(self, text="Vol-", command=volumeDown)
        VOL_DOWN.pack(side=LEFT)
        
        PREVIOUS = Button(self, text="Previous", command=previous)
        PREVIOUS.pack(side=LEFT)
        
        NEXT = Button(self, text="Next", command=next)
        NEXT.pack(side=LEFT)
    
    def __init__(self, master=None):
        Frame.__init__(self, master)
        master.title("BTRMC Host")
        self.createWidgets()
        self.pack()

if __name__ == "__main__":
    root = Tk()
    hwnd = root.winfo_id()
    try:
        thread.start_new_thread(BTServer, ("BTServer",))
    except:
        print "== !! Start Bluetooth Server fail !! =="
        traceback.print_exc()
    try:
        thread.start_new_thread(WifiServer, ("WifiServer",))
    except:
        print "== !! Start Wifi Server fail !! =="
        traceback.print_exc()
    
    frame = MainFrame(master=root)
    frame.mainloop()
    