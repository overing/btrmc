#!/usr/bin/python
# -*- coding:utf-8 -*-

import ctypes
import time

CODES = {
    'VK_ACCEPT':      30,
    'VK_ADD':        107,
    'VK_ALT':         18,
    'VK_APPS':        93,
    'VK_ATTN':       246,
    'VK_BACK':         8,
    'VK_CANCEL':       3,
    'VK_CAPITAL':     20,
    'VK_CLEAR':       12,
    'VK_CONTROL':     17,
    'VK_CONVERT':     28,
    'VK_CRSEL':      247,
    'VK_CTRL':        17,
    'VK_DECIMAL':    110,
    'VK_DELETE':      46,
    'VK_DIVIDE':     111,
    'VK_DOWN':        40,
    'VK_END':         35,
    'VK_ENTER':       13,
    'VK_EREOF':      249,
    'VK_ESCAPE':      27,
    'VK_EXECUTE':     43,
    'VK_EXSEL':      248,
    'VK_F1':         112,
    'VK_F2':         113,
    'VK_F3':         114,
    'VK_F4':         115,
    'VK_F5':         116,
    'VK_F6':         117,
    'VK_F7':         118,
    'VK_F8':         119,
    'VK_F9':         120,
    'VK_F10':        121,
    'VK_F11':        122,
    'VK_F12':        123,
    'VK_F13':        124,
    'VK_F14':        125,
    'VK_F15':        126,
    'VK_F16':        127,
    'VK_F17':        128,
    'VK_F18':        129,
    'VK_F19':        130,
    'VK_F20':        131,
    'VK_F21':        132,
    'VK_F22':        133,
    'VK_F23':        134,
    'VK_F24':        135,
    'VK_FINAL':       24,
    'VK_HANGEUL':     21,
    'VK_HANGUL':      21,
    'VK_HANJA':       25,
    'VK_HELP':        47,
    'VK_HOME':        36,
    'VK_INSERT':      45,
    'VK_JUNJA':       23,
    'VK_KANA':        21,
    'VK_KANJI':       25,
    'VK_LBUTTON':      1,
    'VK_LCONTROL':   162,
    'VK_LEFT':        37,
    'VK_LMENU':      164,
    'VK_LSHIFT':     160,
    'VK_LWIN':        91,
    'VK_MBUTTON':      4,
    'VK_MENU':        18,
    'VK_MODECHANGE':  31,
    'VK_MULTIPLY':   106,
    'VK_NEXT':        34,
    'VK_NONAME':     252,
    'VK_NONCONVERT':  29,
    'VK_NUMLOCK':    144,
    'VK_NUMPAD0':     96,
    'VK_NUMPAD1':     97,
    'VK_NUMPAD2':     98,
    'VK_NUMPAD3':     99,
    'VK_NUMPAD4':    100,
    'VK_NUMPAD5':    101,
    'VK_NUMPAD6':    102,
    'VK_NUMPAD7':    103,
    'VK_NUMPAD8':    104,
    'VK_NUMPAD9':    105,
    'VK_OEM_CLEAR':  254,
    'VK_PA1':        253,
    'VK_PAUSE':       19,
    'VK_PLAY':       250,
    'VK_PRINT':       42,
    'VK_PRIOR':       33,
    'VK_PROCESSKEY': 229,
    'VK_RBUTTON':      2,
    'VK_RCONTROL':   163,
    'VK_RETURN':      13,
    'VK_RIGHT':       39,
    'VK_RMENU':      165,
    'VK_RSHIFT':     161,
    'VK_RWIN':        92,
    'VK_SCROLL':     145,
    'VK_SELECT':      41,
    'VK_SEPARATOR':  108,
    'VK_SHIFT':       16,
    'VK_SNAPSHOT':    44,
    'VK_SPACE':       32,
    'VK_SUBTRACT':   109,
    'VK_TAB':          9,
    'VK_UP':          38,
    
    'VK_0':   0x30,
    'VK_1':   0x31,
    'VK_2':   0x32,
    'VK_3':   0x33,
    'VK_4':   0x34,
    'VK_5':   0x35,
    'VK_6':   0x36,
    'VK_7':   0x37,
    'VK_8':   0x38,
    'VK_9':   0x39,
    'VK_A':   0x41,
    'VK_B':   0x42,
    'VK_C':   0x43,
    'VK_D':   0x44,
    'VK_E':   0x45,
    'VK_F':   0x46,
    'VK_G':   0x47,
    'VK_H':   0x48,
    'VK_I':   0x49,
    'VK_J':   0x4A,
    'VK_K':   0x4B,
    'VK_L':   0x4C,
    'VK_M':   0x4D,
    'VK_N':   0x4E,
    'VK_O':   0x4F,
    'VK_P':   0x50,
    'VK_Q':   0x51,
    'VK_R':   0x52,
    'VK_S':   0x53,
    'VK_T':   0x54,
    'VK_U':   0x55,
    'VK_V':   0x56,
    'VK_W':   0x57,
    'VK_X':   0x58,
    'VK_Y':   0x59,
    'VK_Z':   0x5A,
}

SendInput = ctypes.windll.user32.SendInput

PUL = ctypes.POINTER(ctypes.c_ulong)
class KeyBdInput(ctypes.Structure):
    _fields_ = [("wVk", ctypes.c_ushort),
                ("wScan", ctypes.c_ushort),
                ("dwFlags", ctypes.c_ulong),
                ("time", ctypes.c_ulong),
                ("dwExtraInfo", PUL)]

class HardwareInput(ctypes.Structure):
    _fields_ = [("uMsg", ctypes.c_ulong),
                ("wParamL", ctypes.c_short),
                ("wParamH", ctypes.c_ushort)]

class MouseInput(ctypes.Structure):
    _fields_ = [("dx", ctypes.c_long),
                ("dy", ctypes.c_long),
                ("mouseData", ctypes.c_ulong),
                ("dwFlags", ctypes.c_ulong),
                ("time",ctypes.c_ulong),
                ("dwExtraInfo", PUL)]

class Input_I(ctypes.Union):
    _fields_ = [("ki", KeyBdInput),
                 ("mi", MouseInput),
                 ("hi", HardwareInput)]

class Input(ctypes.Structure):
    _fields_ = [("type", ctypes.c_ulong),
                ("ii", Input_I)]

def PressKey(vk_code):
    extra = ctypes.c_ulong(0)
    ii_ = Input_I()
    ii_.ki = KeyBdInput( vk_code, 0x00, 0x0000, 0, ctypes.pointer(extra) )
    x = Input( ctypes.c_ulong(1), ii_ )
    ctypes.windll.user32.SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))

def ReleaseKey(vk_code):
    extra = ctypes.c_ulong(0)
    ii_ = Input_I()
    ii_.ki = KeyBdInput( vk_code, 0x00, 0x0002, 0, ctypes.pointer(extra) )
    x = Input( ctypes.c_ulong(1), ii_ )
    ctypes.windll.user32.SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))

def TypeKey(unicode):
    extra = ctypes.c_ulong(0)
    ii_ = Input_I()
    ii_.ki = KeyBdInput( 0, unicode, 0x0004, 0, ctypes.pointer(extra) )
    x = Input( ctypes.c_ulong(1), ii_ )
    ctypes.windll.user32.SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))

def ActionVirtualKey(vk_name):
    vkc = CODES[vk_name]
    PressKey(vkc)
    time.sleep(.1)
    ReleaseKey(vkc)

def TypeString(string):
    for char in string:
        unicode = ord(char.decode("utf-8"))
        TypeKey(unicode)
        time.sleep(.1)

def typeTest():
    time.sleep(1)
    ActionVirtualKey("VK_LWIN")
    
    time.sleep(.25)
    TypeString("notepad.exe")
    
    time.sleep(.5)
    ActionVirtualKey("VK_ENTER")
    
    time.sleep(.25)
    TypeString("Hello World!")

if __name__ =="__main__":
    typeTest()
