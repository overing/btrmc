package org.overwork.btrmc;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public abstract class Connector extends AsyncTask<Void, Void, Boolean> {

    protected MainActivity mActivity;
    protected Exception mEX;

    private static final String TAG = Connector.class.getSimpleName();

    private ProgressDialog mPD;

    protected Connector(MainActivity activity) {
	mActivity = activity;
    }

    @Override
    protected void onPreExecute() {
	String message = "Connection...";
	mPD = ProgressDialog.show(mActivity, null, message, false, false);
    }

    @Override
    protected void onPostExecute(Boolean result) {
	mPD.dismiss();
	if (result == false) {
	    String text = "Connect fail";
	    if (mEX != null) {
		text += ":\n" + mEX.getLocalizedMessage();
	    }
	    Toast.makeText(mActivity, text, Toast.LENGTH_LONG).show();
	    mActivity.showHostChooser();
	}
    }

    public static class Bluetooth extends Connector {

	private BluetoothDevice mDevice;

	public Bluetooth(MainActivity activity, BluetoothDevice device) {
	    super(activity);
	    mDevice = device;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
	    try {
		Client.Bluetooth client = new Client.Bluetooth();
		client.connect(mDevice);
		mActivity.setClient(client);
		return true;
	    } catch (Exception ex) {
		Log.e(TAG, "on connect hostr fail", ex);
		mEX = ex;
		return false;
	    }
	}
    }

    public static class Wifi extends Connector {

	private String mHost;
	private int mPort;

	public Wifi(MainActivity activity, String host, int port) {
	    super(activity);
	    mHost = host;
	    mPort = port;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
	    try {
		Client.Wifi client = new Client.Wifi();
		client.connect(mHost, mPort);
		mActivity.setClient(client);
		mActivity.saveWifiConf(mHost, mPort);
		return true;
	    } catch (Exception ex) {
		Log.e(TAG, "on connect hostr fail", ex);
		mEX = ex;
		return false;
	    }
	}
    }
}
