package org.overwork.btrmc;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements //
	View.OnClickListener, //
	DialogInterface.OnClickListener, //
	Client.OnDisconnectedListener {

    private Client mClient;

    private BluetoothAdapter mBluetoothAdapter;

    private SharedPreferences mPreferences;

    private BluetoothDeviceAdapter mBluetoothDeviceAdapter;

    private AlertDialog mDailog_BTH, mDailog_WH;

    private EditText mET_WifiHost, mET_WifiPort;

    @Override
    public void onBackPressed() {
	if (mClient != null) {
	    mClient.setDisconnectedListener(null);
	}
	super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
	try {
	    if (v.getId() == R.id.btn_play_pause) {
		mClient.send("CMD_PLAY_PAUSE".getBytes());
	    } else if (v.getId() == R.id.btn_stop) {
		mClient.send("CMD_STOP".getBytes());
	    } else if (v.getId() == R.id.btn_previous) {
		mClient.send("CMD_PREVIOUS".getBytes());
	    } else if (v.getId() == R.id.btn_next) {
		mClient.send("CMD_NEXT".getBytes());
	    } else if (v.getId() == R.id.btn_vol_down) {
		mClient.send("CMD_VOL_DOWN".getBytes());
	    } else if (v.getId() == R.id.btn_vol_up) {
		mClient.send("CMD_VOL_UP".getBytes());
	    }
	} catch (IOException ex) {
	    String text = "Send command fail:\n" + ex.toString();
	    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
	}
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
	if (dialog == mDailog_BTH) {
	    if (which == DialogInterface.BUTTON_POSITIVE) {
		startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
	    } else if (which == DialogInterface.BUTTON_NEUTRAL) {
		mDailog_WH.show();
	    } else if (which == DialogInterface.BUTTON_NEGATIVE) {
		setResult(RESULT_CANCELED);
		finish();
	    } else {
		BluetoothDevice device = mBluetoothDeviceAdapter.getItem(which);
		new Connector.Bluetooth(this, device).execute();
	    }
	} else if (dialog == mDailog_WH) {
	    if (which == DialogInterface.BUTTON_POSITIVE) {
		String host = mET_WifiHost.getText().toString();
		int port;
		try {
		    port = Integer.parseInt(mET_WifiPort.getText().toString());
		    if (TextUtils.isEmpty(host)) {
			String text = "Hostname is empty";
			Toast.makeText(this, text, Toast.LENGTH_LONG).show();
			mDailog_WH.show();
			mET_WifiHost.requestFocus();
		    } else {
			new Connector.Wifi(this, host, port).execute();
		    }
		} catch (NumberFormatException nfex) {
		    String text = "Port number format fail";
		    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
		    mDailog_WH.show();
		    mET_WifiPort.requestFocus();
		}
	    } else if (which == DialogInterface.BUTTON_NEUTRAL) {
		buildBTHostDialog().show();
	    } else if (which == DialogInterface.BUTTON_NEGATIVE) {
		setResult(RESULT_CANCELED);
		finish();
	    }
	}
    }

    @Override
    public void onDisconnected() {
	runOnUiThread(new Runnable() {

	    @Override
	    public void run() {
		Toast.makeText(MainActivity.this, "Disconnected",
			Toast.LENGTH_LONG).show();
		showHostChooser();
	    }
	});
    }

    public void showHostChooser() {
	if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
	    buildBTHostDialog().show();
	} else {
	    mDailog_WH.show();
	}
    }

    public void setClient(Client client) {
	mClient = client;
	mClient.setDisconnectedListener(this);
    }

    public void saveWifiConf(String host, int port) {
	SharedPreferences.Editor pe = mPreferences.edit();
	pe.putString("wifi_host", host);
	pe.putInt("wifi_port", port);
	pe.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);

	mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	mPreferences = getPreferences(MODE_PRIVATE);

	View view = View.inflate(this, R.layout.dialog_wifi_connect, null);
	mET_WifiHost = (EditText) view.findViewById(R.id.et_host);
	mET_WifiPort = (EditText) view.findViewById(R.id.et_port);
	AlertDialog.Builder adb = new AlertDialog.Builder(this);
	adb.setCancelable(false);
	adb.setTitle("Input wifi host");
	adb.setView(view);
	adb.setPositiveButton("Connect", this);
	adb.setNeutralButton("Use bluetooth", this);
	adb.setNegativeButton(android.R.string.cancel, this);
	mDailog_WH = adb.create();

	mET_WifiHost.setText(mPreferences.getString("wifi_host", ""));
	mET_WifiPort.setText(mPreferences.getInt("wifi_port", 8080) + "");

	findViewById(R.id.btn_play_pause).setOnClickListener(this);
	findViewById(R.id.btn_stop).setOnClickListener(this);
	findViewById(R.id.btn_previous).setOnClickListener(this);
	findViewById(R.id.btn_next).setOnClickListener(this);
	findViewById(R.id.btn_vol_down).setOnClickListener(this);
	findViewById(R.id.btn_vol_up).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
	super.onResume();
	if (mClient == null || mClient.isConnected() == false) {
	    showHostChooser();
	}
    }

    @Override
    protected void onDestroy() {
	try {
	    if (mClient != null) {
		mClient.close();
	    }
	} catch (IOException ignore) {
	}
	super.onDestroy();
    }

    private AlertDialog buildBTHostDialog() {
	AlertDialog.Builder adb = new AlertDialog.Builder(this);
	adb.setCancelable(false);
	adb.setTitle("Choose bluetooth host");

	if (mBluetoothAdapter == null) {
	    adb.setMessage("<Unsupported bluetooth>");
	} else {
	    mBluetoothDeviceAdapter = new BluetoothDeviceAdapter(
		    mBluetoothAdapter.getBondedDevices());
	    if (mBluetoothDeviceAdapter.getCount() > 0) {
		adb.setAdapter(mBluetoothDeviceAdapter, this);
	    } else {
		if (mBluetoothAdapter.isEnabled()) {
		    adb.setMessage("<No bonded item>");
		} else {
		    adb.setMessage("<Bluetooth is disable>");
		}
	    }
	}

	adb.setPositiveButton("Setting", this);
	adb.setNeutralButton("Use Wifi", this);
	adb.setNegativeButton(android.R.string.cancel, this);
	return mDailog_BTH = adb.create();
    }
}
