package org.overwork.btrmc;

import java.util.Collection;

import android.bluetooth.BluetoothDevice;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class BluetoothDeviceAdapter extends BaseAdapter {

    private BluetoothDevice[] mDevices;

    public BluetoothDeviceAdapter(Collection<BluetoothDevice> devices) {
	this(devices.toArray(new BluetoothDevice[devices.size()]));
    }

    public BluetoothDeviceAdapter(BluetoothDevice[] devices) {
	mDevices = devices;
    }

    @Override
    public int getCount() {
	return mDevices.length;
    }

    @Override
    public BluetoothDevice getItem(int position) {
	return mDevices[position];
    }

    @Override
    public long getItemId(int position) {
	return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	if (convertView == null) {
	    convertView = View.inflate(parent.getContext(),
		    android.R.layout.simple_list_item_2, null);
	}
	BluetoothDevice device = getItem(position);
	((TextView) convertView.findViewById(android.R.id.text1))
		.setText(device.getName());
	((TextView) convertView.findViewById(android.R.id.text2))
		.setText(device.getAddress());
	return convertView;
    }
}
