package org.overwork.btrmc;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Build;
import android.util.Log;

public abstract class Client implements Closeable, Runnable {

    private static final String TAG = Client.class.getSimpleName();

    private Thread mThread;

    protected InputStream mInput;
    protected OutputStream mOutput;

    private OnDisconnectedListener mOnDisconnectedListener;

    @Override
    public void run() {
	int read;
	byte[] buffer = new byte[512];
	while (!Thread.interrupted()) {
	    try {
		read = mInput.read(buffer);
		Log.w(TAG, "rec: " + new String(buffer, 0, read));
	    } catch (IOException ex) {
		Log.w(TAG, "read fail", ex);
		if (mOnDisconnectedListener != null) {
		    mOnDisconnectedListener.onDisconnected();
		}
		break;
	    }
	}
    }

    @Override
    public void close() throws IOException {
	mThread.interrupt();

	try {
	    mInput.close();
	} catch (IOException ignore) {
	}

	try {
	    mOutput.close();
	} catch (IOException ignore) {
	}
    }

    public void setDisconnectedListener(OnDisconnectedListener listener) {
	mOnDisconnectedListener = listener;
    }

    public abstract boolean isConnected();

    public void send(byte[] data) throws IOException {
	mOutput.write(data);
    }

    protected void startRead() {
	mThread = new Thread(this);
	mThread.setDaemon(false);
	mThread.start();
    }

    public interface OnDisconnectedListener {
	void onDisconnected();
    }

    public static class Bluetooth extends Client {

	private static final UUID UUID_BT_SPP = UUID
		.fromString("00001101-0000-1000-8000-00805F9B34FB");

	private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
		.getDefaultAdapter();

	private BluetoothSocket mBluetoothSocket;

	public void connect(BluetoothDevice device) throws IOException {
	    if (mBluetoothAdapter.isDiscovering()) {
		mBluetoothAdapter.cancelDiscovery();
	    }

	    mBluetoothSocket = device
		    .createRfcommSocketToServiceRecord(UUID_BT_SPP);

	    mBluetoothSocket.connect();
	    mInput = mBluetoothSocket.getInputStream();
	    mOutput = mBluetoothSocket.getOutputStream();
	    startRead();
	}

	@Override
	public void close() throws IOException {
	    super.close();
	    try {
		mBluetoothSocket.close();
	    } catch (IOException ignore) {
	    }
	}

	@SuppressLint("NewApi")
	@Override
	public boolean isConnected() {
	    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
		return mBluetoothSocket.isConnected();
	    } else {
		try {
		    mBluetoothSocket.getOutputStream();
		    return true;
		} catch (IOException ex) {
		    return false;
		}
	    }
	}
    }

    public static class Wifi extends Client {

	private Socket mSocket;

	public void connect(String host, int port) throws Exception {
	    mSocket = new Socket();
	    mSocket.connect(new InetSocketAddress(host, port), 6000);
	    mInput = mSocket.getInputStream();
	    mOutput = mSocket.getOutputStream();
	    startRead();
	}

	@Override
	public void close() throws IOException {
	    super.close();
	    try {
		mSocket.close();
	    } catch (IOException ignore) {
	    }
	}

	@Override
	public boolean isConnected() {
	    return mSocket.isConnected();
	}
    }
}
